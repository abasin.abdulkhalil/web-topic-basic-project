// imports
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { AmmoPhysics, PhysicsLoader } from '@enable3d/ammo-physics'

const MainScene = () => {

  // veriablen
  var camera, scene, renderer, mesh, goal, keys, follow, mixerPlane;
  var temp = new THREE.Vector3;
  var dir = new THREE.Vector3;
  var a = new THREE.Vector3;
  var b = new THREE.Vector3;
  var distance = 0.5;
  var velocity = 0.0;
  var speed = 0.0;
  var loader = new GLTFLoader();
  const fontLoader =  new THREE.FontLoader();

  // scene
  scene = new THREE.Scene()
  scene.background = new THREE.Color(0x00E4FF)

  // camera
  camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 10000)
  camera.position.set(3.5, 5, 5)
  camera.lookAt(scene.position)

  // renderer
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(window.innerWidth, window.innerHeight)
  renderer.shadowMap.enabled = true
  renderer.autoClear = false
  document.body.appendChild(renderer.domElement)

  // device pixel ratio voor scherpe 3d modellen
  const DPR = window.devicePixelRatio
  renderer.setPixelRatio(Math.min(2 , DPR))

  // orbit controls
  // new OrbitControls(camera, renderer.domElement)

  // light
  scene.add(new THREE.HemisphereLight(0xffffff, 0x080820, .2))
  scene.add(new THREE.AmbientLight(0x666666))
  const light = new THREE.DirectionalLight(0xdfebff, 1)
  light.position.set(100, 500, 100)
  light.position.multiplyScalar(1.3)
  scene.add(light)

  var lightBlokjes = new THREE.DirectionalLight(0x444444, -1)
  lightBlokjes.position.set(-100, 300, -100)
  lightBlokjes.castShadow = true
  scene.add(lightBlokjes)
  lightBlokjes.shadow.camera.top += 100
  lightBlokjes.shadow.camera.bottom -= 100
  lightBlokjes.shadow.camera.left -= 100
  lightBlokjes.shadow.camera.right += 100
  lightBlokjes.shadow.mapSize.set(5000,5000)

  // scene.add(new THREE.CameraHelper(lightBlokjes.shadow.camera))
  // const helper = new THREE.DirectionalLightHelper( lightBlokjes, 5 );
  // scene.add( helper );

  // physics instent maken
  const physics = new AmmoPhysics(scene as any)
  // physics.debug?.enable()


  // grond
  makeGround()

  // mijn logo
  makeLogo()
  
  // blokjes
  makeBlocks()
  
  // tegels voor de vliegtuig
  makePath(5 , 0, 5)
  makePath(5.2 , 0, 5.7)
  makePath(5 , 0, 6.4)
  // tegels naar skills
  makePath(4 , 0, 18)
  makePath(4.2 , 0, 18.7)
  makePath(4 , 0, 19.4)
  // tegels naar projects
  makePath( 0, 0, 25)
  makePath(0.2 , 0, 25.7)
  makePath(0 , 0, 26.4)
  makePath( 0.2, 0, 27.1)
  makePath(0 , 0, 27.8)
  makePath(0.2 , 0, 28.5)
  makePath(0 , 0, 29.2)

  makePath(-2 , 0, 34.8)
  makePath(-2.2 , 0, 35.5)
  makePath(-2, 0, 36.2)

  // Mijn naam, skills, project sne the end woorden: elk letter is apart gemaakt zo dat ze elk hun eigen physics hebben
  makeCaptions()
  
  // mijn foto
  makeAbasinPic()

  // mijn biografie
  makeAbasinBio()
  
  // mijn skills
  makeSkills()

  // laad de gltf modellen
  loadGLtfs()
  
  // kleine planten
  loadSmallPlants(4, 0.5, 5.4)
  loadSmallPlants(6, 0.5, 5.4)

  loadSmallPlants(3, 0.5, 19)
  loadSmallPlants(5, 0.5, 19)

  loadSmallPlants(-1.3, 0.5, 25.7)
  loadSmallPlants(1.5, 0.5, 25.7)
  loadSmallPlants(-1.3, 0.5, 28)
  loadSmallPlants(1.5, 0.5, 28)

  loadSmallPlants(-3.2, 0.5, 35.5)
  loadSmallPlants(-1, 0.5, 35.5)

  // projecten
  makeProject('transport-banner', 'transport', 4)
  projectInfo('transport-txt', 4)

  makeProject('vc-banner', 'vcok', 8)
  projectInfo('vc-txt', 8)

  makeProject('un-banner', 'universe', -8)
  projectInfo('un-txt', -8)

  makeProject('tp-banner', 'trajectplanner', -12)
  projectInfo('tp-txt', -12)
    
// Camera moet de vliegtuig volgen
  goal = new THREE.Object3D;
  follow = new THREE.Object3D;
  follow.position.z = -distance;
  mesh.add( follow );
  goal.add( camera );
  scene.add( mesh );

  keys = {
    arrowup: false,
    arrowdown: false,
    arrowright: false,
    arrowleft: false
  };
  
  document.body.addEventListener( 'keydown', function(e) {
    const key = e.code.replace('Key', '').toLowerCase();
    if ( keys[ key ] !== undefined )
      keys[ key ] = true;
      //console.log(key);
  });
  document.body.addEventListener( 'keyup', function(e) {
    const key = e.code.replace('Key', '').toLowerCase();
    if ( keys[ key ] !== undefined )
      keys[ key ] = false;
  });

  // clock
  const clock = new THREE.Clock()

  // Vliegtuig posities na de update
  const animate = () => {
    speed = 0.0;
    if ( keys.arrowup ){
      speed = 0.09
    }
    else if ( keys.arrowdown ){
      speed = -0.01
    }

    velocity += ( speed - velocity ) * .3;
    mesh.translateZ( velocity )

    if ( keys.arrowleft ){
      mesh.rotateY(0.05)
    }
    else if ( keys.arrowright ){
      mesh.rotateY(-0.05)
    }
    a.lerp(mesh.position, 0.4)
    b.copy(goal.position)
    dir.copy( a ).sub( b ).normalize()
    const dis = a.distanceTo( b ) - distance;
    goal.position.addScaledVector( dir, dis )
    goal.position.lerp(temp, 0.02);
    temp.setFromMatrixPosition(follow.matrixWorld)
    camera.lookAt( mesh.position )
    mesh.body.needUpdate = true

    // de zwaartekracht in de 3d wereld
    physics.update(clock.getDelta() * 2000)
    physics.updateDebugger()

    // snelheid van de vliegtuig animatie
    if ( mixerPlane ) mixerPlane.update( clock.getDelta()*100)

    // animtaties renderen
    // renderer.clear()
    renderer.render(scene, camera)
    //renderer.clearDepth()
    requestAnimationFrame(animate)
  }
  requestAnimationFrame(animate)

  // update de camera en scene als de venster geresized is
  window.addEventListener( 'resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  }, false );

  // clickable maken
  const raycaster = new THREE.Raycaster();
  const mouseClick = new THREE.Vector2();
  const mouseMove = new THREE.Vector2();
  var clickable = THREE.Object3D;

  window.addEventListener('click', event => {
    mouseClick.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	  mouseClick.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    raycaster.setFromCamera( mouseClick, camera );
    const projects = raycaster.intersectObjects( scene.children );
    if (projects.length > 0 && projects[0].object.userData.clickable) {
      clickable = projects[0].object
      if (clickable.userData.name === 'trajectplanner') window.location.href = 'https://trajectplanner.abasin.space/planner/#/'
      if (clickable.userData.name === 'vcok') window.location.href = 'https://abasin.space/webdesign/vcok/index.html'
      if (clickable.userData.name === 'universe') window.location.href = 'https://abasin.space/webdesign/universe/index.html'
      if (clickable.userData.name === 'transport') window.location.href = 'https://abasin.space/webdesign/moon/index.html'
    }
  })



  function makeGround() {
    physics.add.ground({ width: 1000, height: 1000, })
    const ground = new THREE.Mesh(new THREE.BoxBufferGeometry(1000, 1.1, 1000), new THREE.MeshLambertMaterial({ color: 0x043927 }));
    ground.receiveShadow = true
    scene.add(ground);
  }


  function makeLogo(){
    const logopng = new THREE.TextureLoader().load('assets/logo.png')
    const logo = new THREE.Mesh(new THREE.BoxGeometry( 0.6, 0.6, 0.6), new THREE.MeshBasicMaterial({ map: logopng}))
    logo.position.set(9, 10, 3)
    logo.castShadow = true
    scene.add(logo)
    physics.add.existing(logo)
    logo.body.setGravity(0,-10,0)
  }


  function makeBlocks(){
    const material = new THREE.MeshLambertMaterial({ color: 0xedaa95})
    mesh = new THREE.Mesh(new THREE.BoxBufferGeometry(0.4, 0.4, 0.6), material)
    mesh.position.set(-3, 0.7, 6)
    mesh.receiveShadow = true
    mesh.castShadow = true
    scene.add(mesh);
    physics.add.existing(mesh)

    // blokjes zetten op de z as
    makeBlocksX(-2, 0.7, 3)
    makeBlocksX(-2.1, 1.1, 2.9)
    makeBlocksX(-2.2, 1.5, 3)
    // blokjes zetten op de x as
    makeBlocksZ(-2.3, 0.7, 2.9)
    makeBlocksZ(-2.4, 1.5, 2.85)
    makeBlocksZ(-2.4, 1.9, 2.8)

    // blokjes zetten op de z as
    makeBlocksX(10, 0.7, -4)
    makeBlocksX(10.1, 1.1, -3.9)
    makeBlocksX(10.2, 1.5, -4)
    // blokjes zetten op de x as
    makeBlocksZ(6.3, 0.7, -3.9)
    makeBlocksZ(6.4, 1.5, -3.85)
    makeBlocksZ(6.4, 1.9, -3.8)

    // blokjes zetten op de x as
    makeBlocksZ(-2.3, 0.7, 9.9)
    makeBlocksZ(-2.4, 1.5, 9.85)
    makeBlocksZ(-2.4, 1.9, 9.8)

    // blokjes zetten op de z as
    makeBlocksX(7, 0.7, 15.9)
    makeBlocksX(7.1, 1.1, 15.85)
    makeBlocksX(7.2, 1.5, 15.8)
    // blokjes zetten op de x as
    makeBlocksZ(7.2, 0.7, 18.85)
    makeBlocksZ(7.3, 1.5, 18.80)
    makeBlocksZ(7.3, 1.9, 18.75)

    // // blokjes zetten op de x as
    // makeBlocksZ(-5.3, 0.7, 32.9)
    // makeBlocksZ(-5.4, 1.5, 32.85)
    // makeBlocksZ(-5.4, 1.9, 32.8)

    // // blokjes zetten op de z as
    // makeBlocksX(-5, 0.7, 32)
    // makeBlocksX(-5.1, 1.1, 32.9)
    // makeBlocksX(-5.2, 1.5, 33)
  }

  function makeBlocksX (bx, by, bz){
    let y = 0
    for(let i = 0; i < 4; i++){
      y += 0.63
      var blokeje = mesh.clone()
      blokeje.position.set(bx, by, bz + y)
      scene.add(blokeje)
      physics.add.existing(blokeje)
    }
  }

  function makeBlocksZ (bx, by, bz){
    let z = 0
    for(let i = 0; i < 5; i++){
      z += 0.63
      var blokeje = new THREE.Mesh(new THREE.BoxBufferGeometry(0.6, 0.4, 0.4),
      new THREE.MeshPhongMaterial({ color: 0xedaa95}))
      blokeje.position.set(bx + z, by, bz)
      blokeje.receiveShadow = true
      blokeje.castShadow = true
      scene.add(blokeje)
      physics.add.existing(blokeje)
    }
  }


  function makePath(x, y, z){
    var path1 = new THREE.Mesh(new THREE.BoxBufferGeometry(0.5, 1.2, 0.5), new THREE.MeshLambertMaterial({ color: 0xedaa95}))
    path1.position.set(x , y, z)
    path1.receiveShadow = true
    scene.add(path1)
  }


  function makeCaptions(){
    fontLoader.load('assets/fonts/Roboto.json', function (font: THREE.Font) {

      // ABASIN
      const textA = new THREE.Mesh(new THREE.TextGeometry('A', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      textA.position.set(6 , 0.6, 7)
      physics.add.existing(textA, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      textA.castShadow = true
      scene.add(textA);
  
      const textB = new THREE.Mesh(new THREE.TextGeometry('B', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      textB.position.set(6.8 , 0.6, 7)
      physics.add.existing(textB, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      textB.castShadow = true
      scene.add(textB);
  
      var textA2 = textA.clone()
      textA2.position.set(7.5 , 0.6, 7)
      physics.add.existing(textA2, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      scene.add(textA2)
  
      const textS = new THREE.Mesh(new THREE.TextGeometry('S', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      textS.position.set(8.3 , 0.6, 7)
      physics.add.existing(textS, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      textS.castShadow = true
      scene.add(textS);
  
      const textI = new THREE.Mesh(new THREE.TextGeometry('I', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      textI.position.set(9 , 0.6, 7)
      physics.add.existing(textI, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      textI.castShadow = true
      scene.add(textI);
  
      const textN = new THREE.Mesh(new THREE.TextGeometry('N', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      textN.position.set(9.4 , 0.6, 7)
      physics.add.existing(textN, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      textN.castShadow = true
      scene.add(textN);
  
  
      // SKILLS
      const skills_S = new THREE.Mesh(new THREE.TextGeometry('S', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      skills_S.position.set(4 , 0.6, 22)
      physics.add.existing(skills_S, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      skills_S.castShadow = true
      scene.add(skills_S)
  
      const skills_K = new THREE.Mesh(new THREE.TextGeometry('K', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      skills_K.position.set(4.7 , 0.6, 22)
      physics.add.existing(skills_K, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      skills_K.castShadow = true
      scene.add(skills_K)
  
      const skills_I = textI.clone()
      skills_I.position.set(5.5, 0.6, 22)
      physics.add.existing(skills_I, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      scene.add(skills_I)
  
      const skills_L = new THREE.Mesh(new THREE.TextGeometry('L', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      skills_L.position.set(6 , 0.6, 22)
      physics.add.existing(skills_L, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      skills_L.castShadow = true
      scene.add(skills_L)
  
      const skills_L2 = skills_L.clone()
      skills_L2.position.set(6.7 , 0.6, 22)
      physics.add.existing(skills_L2, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      scene.add(skills_L2)
  
      const skills_S2 = skills_S.clone()
      skills_S2.position.set(7.3 , 0.6, 22)
      physics.add.existing(skills_S2, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      scene.add(skills_S2)

      
      // PROJECTS
      const projects_P = new THREE.Mesh(new THREE.TextGeometry('P', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_P.position.set(-6 , 0.6, 32)
      physics.add.existing(projects_P, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_P.castShadow = true
      scene.add(projects_P)

      const projects_R = new THREE.Mesh(new THREE.TextGeometry('R', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_R.position.set(-7 , 0.6, 32)
      physics.add.existing(projects_R, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_R.castShadow = true
      scene.add(projects_R)

      const projects_O = new THREE.Mesh(new THREE.TextGeometry('O', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_O.position.set(-5.2 , 0.6, 32)
      physics.add.existing(projects_O, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_O.castShadow = true
      scene.add(projects_O)

      const projects_J = new THREE.Mesh(new THREE.TextGeometry('J', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_J.position.set(-4.4 , 0.6, 32)
      physics.add.existing(projects_J, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_J.castShadow = true
      scene.add(projects_J)

      const projects_E = new THREE.Mesh(new THREE.TextGeometry('E', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_E.position.set(-3.7 , 0.6, 32)
      physics.add.existing(projects_E, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_E.castShadow = true
      scene.add(projects_E)

      const projects_C = new THREE.Mesh(new THREE.TextGeometry('C', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_C.position.set(-2.9 , 0.6, 32)
      physics.add.existing(projects_C, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_C.castShadow = true
      scene.add(projects_C)

      const projects_T = new THREE.Mesh(new THREE.TextGeometry('T', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      projects_T.position.set(-2.1 , 0.6, 32)
      physics.add.existing(projects_T, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_T.castShadow = true
      scene.add(projects_T)

      const projects_S = skills_S.clone()
      projects_S.position.set(-1.4 , 0.6, 32)
      physics.add.existing(projects_S, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      projects_S.castShadow = true
      scene.add(projects_S)


      // THE END
      const end_T = projects_T.clone()
      end_T.position.set(-5 , 0.6, 43)
      physics.add.existing(end_T, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_T.castShadow = true
      scene.add(end_T)

      const end_H = new THREE.Mesh(new THREE.TextGeometry('H', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      end_H.position.set(-4.2 , 0.6, 43)
      physics.add.existing(end_H, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_H.castShadow = true
      scene.add(end_H)

      const end_E = projects_E.clone()
      end_E.position.set(-3.25 , 0.6, 43)
      physics.add.existing(end_E, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_E.castShadow = true
      scene.add(end_E)

      const end_E2 = projects_E.clone()
      end_E2.position.set(-2 , 0.6, 43)
      physics.add.existing(end_E2, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_E2.castShadow = true
      scene.add(end_E2)

      const end_N = textN.clone()
      end_N.position.set(-1.2 , 0.6, 43)
      physics.add.existing(end_N, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_N.castShadow = true
      scene.add(end_N)

      const end_D = new THREE.Mesh(new THREE.TextGeometry('D', {font: font,size: 0.8,height: 0.2}), [
        new THREE.MeshPhongMaterial({color: 'white'}),
        new THREE.MeshPhongMaterial({color: 0xbcbcbc})
      ])
      end_D.position.set(-0.3 , 0.6, 43)
      physics.add.existing(end_D, {shape: 'box', width:0.5, height:0.5, depth: 0.2})
      end_D.castShadow = true
      scene.add(end_D)
    })
  }


  function makeAbasinPic(){
    const abasinjpg = new THREE.TextureLoader().load('assets/abasin.jpg')
    const abasin = new THREE.Mesh(new THREE.BoxGeometry( 2, 0.1, 2), new THREE.MeshBasicMaterial({ map: abasinjpg}))
    abasin.position.set(7, 1, 9)
    scene.add(abasin)
    physics.add.existing(abasin, { shape: 'box', width:2, height:0.3, depth: 2})
    abasin.body.setGravity(0,-100,0)
  }


  function makeAbasinBio(){
    const biopng = new THREE.TextureLoader().load('assets/bio.png')
    const bio = new THREE.Mesh(new THREE.BoxGeometry( 3.3, 0.1, 5), new THREE.MeshBasicMaterial({ map: biopng, transparent: true}))
    bio.position.set(7.6, 0.52, 12.8)
    scene.add(bio)
  }


  function makeSkills(){
    const skill1png = new THREE.TextureLoader().load('assets/skill1.png')
    const skill1 = new THREE.Mesh(new THREE.BoxGeometry( 4, 1, 4), new THREE.MeshBasicMaterial({ map: skill1png}))
    skill1.position.set(6 , 0.07, 25.1)
    skill1.receiveShadow = true
    scene.add(skill1)

    const skill2png = new THREE.TextureLoader().load('assets/skill2.png')
    const skill2 = new THREE.Mesh(new THREE.BoxGeometry( 4, 1, 4), new THREE.MeshBasicMaterial({ map: skill2png}))
    skill2.position.set(6 , 0.07, 29.1)
    skill1.receiveShadow = true
    scene.add(skill2)
  }


  function loadSmallPlants(x, y, z){
    loader.load( 'assets/plants/scene.gltf', function ( gltf ) {
      var plants = gltf.scene
      plants.position.set(x, y, z)
      plants.scale.set(.3,.3,.3)
      // activeer de schaduw
      plants.traverse(o => {
        if(o.isMesh){
          o.castShadow = true
          o.receiveShadow = true
        }
      })
      scene.add( plants );
    });
  }


  function makeProject(photo, name, x_position){
    // top
    const material = new THREE.MeshLambertMaterial({ color: 0xADA7A7 })
    const top = new THREE.Mesh(new THREE.BoxBufferGeometry(2, 1.5, 0.1), new THREE.MeshLambertMaterial({ color: 0x444444 }))
    top.position.set(0, 0.9, -0.85)
    top.rotation.set(-0.2, 0, 0)
    top.castShadow = true
    // screen
    const banner = new THREE.TextureLoader().load('assets/'+ photo +'.png')
    const screen = new THREE.Mesh(new THREE.BoxBufferGeometry(1.8, 1.3, 0.06), new THREE.MeshBasicMaterial({ map: banner}))
    screen.position.set(0, 0, 0.03)
    top.add(screen)
    // keyboard
    const clickme = new THREE.TextureLoader().load('assets/clickme.png')
    const keyboard = new THREE.Mesh(new THREE.BoxBufferGeometry(1, 0.22, 0.8), new THREE.MeshBasicMaterial({ map: clickme}))
    // bottom
    const bottom = new THREE.Mesh(new THREE.BoxBufferGeometry(2, 0.2, 1.5), material)
    bottom.position.set(5, 2, 5)
    bottom.add(top)
    bottom.add(keyboard)
    scene.add(bottom)
    bottom.position.set(x_position, 1, 35)
    // bottom.rotation.set(0, 1.5, 0)

    bottom.userData.clickable = true
    bottom.userData.name = name
    bottom.castShadow = true
    bottom.receiveShadow = true
    physics.add.existing(bottom)
    bottom.body.setGravity(0,-20,0)
    
  }

  function projectInfo(textpng, x_position){
    const txtpng = new THREE.TextureLoader().load('assets/' + textpng + '.png')
    const txt = new THREE.Mesh(new THREE.BoxGeometry( 3.3, 0.1, 5), new THREE.MeshBasicMaterial({ map: txtpng, transparent: true}))
    txt.position.set(x_position, 0.52, 38.5)
    scene.add(txt)
  }


  function loadGLtfs(){
    // vliegtuig
    loader.load( 'assets/plane/scene.gltf', function ( gltf ) {
      mesh = gltf.scene;
      mesh.scale.set(.002,.002,.002);
      mesh.position.set(5, 0.9, 3)
      scene.add( mesh );

      // activeer de schaduw
      mesh.traverse(o => {
        if(o.isMesh){
          o.castShadow = true
          o.receiveShadow = true
        }
      })
      physics.add.existing(mesh, {shape: 'convex'})
      mesh.body.setCollisionFlags(2) // make it kinematic

      // animatie van de vliegtuig laden
      mixerPlane = new THREE.AnimationMixer( gltf.scene );
        gltf.animations.forEach( ( clip ) => {
            mixerPlane.clipAction( clip ).play();
        } );
    });

    // auto
    loader.load( 'assets/car1/scene.gltf', function ( gltf ) {
      var car = gltf.scene;
      car.position.set(9, 0, 3)
      car.rotation.y = 29;
      car.scale.set(.003,.003,.003);

      // activeer de schaduw
      car.traverse(o => {
        if(o.isMesh){
          o.castShadow = true
          o.receiveShadow = true
        }
      })
      scene.add( car );
      physics.add.existing(car, {shape: 'convex', collisionFlags: 0})

    });

    // grote planten
    loader.load( 'assets/plants2/scene.gltf', function ( gltf ) {
      var plants = gltf.scene
      plants.position.set(0, 0.5, 16)
      plants.scale.set(.6,.6,.6)
      // activeer de schaduw
      plants.traverse(o => {
        if(o.isMesh){
          o.castShadow = true
          o.receiveShadow = true
        }
      })
      scene.add( plants );
    });
  
    // portal
    loader.load( 'assets/portal/scene.gltf', function ( gltf ) {
      var object = gltf.scene
      object.position.set(-8, 0.5, 26)
      object.scale.set(1, 1, 1)
      object.rotation.y = Math.PI / 2;
      // activeer de schaduw
      object.traverse(o => {
        if(o.isMesh){
          o.castShadow = true
          o.receiveShadow = true
        }
      })
      scene.add( object );
      physics.add.existing(object, {shape: 'convex'})
      object.body.setGravity(0, -100, 0)
    });
  }
}

PhysicsLoader('/ammo', () => MainScene())
