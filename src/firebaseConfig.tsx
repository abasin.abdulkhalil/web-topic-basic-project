// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAEsgeZQ3yNywtTGqGUcUymhF3fnATYsn4",
  authDomain: "abasin-portfolio.firebaseapp.com",
  projectId: "abasin-portfolio",
  storageBucket: "abasin-portfolio.appspot.com",
  messagingSenderId: "424337867820",
  appId: "1:424337867820:web:2dae9b8e53093864627d89",
  measurementId: "G-4179E9Z8ND"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);